﻿using UnityEngine;

public class VerticesData : MonoBehaviour
{
    [HideInInspector] public Vector3[] generatedVertices;
}
