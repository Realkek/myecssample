﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DamageData : Data
{
    [SerializeField] private float damage;
}