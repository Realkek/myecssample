﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DetectingData : Data
{
    public Collider detectingBoxCollider;
}