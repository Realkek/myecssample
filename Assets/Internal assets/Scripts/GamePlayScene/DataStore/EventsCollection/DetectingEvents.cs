﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DetectingEvents
{
    public const string PlayerHasBeenDetected = "PlayerHasBeenDetected";
    public const string EntityDetectingColliderTriggered = "EntityDetectingColliderTriggered";
    public const string EntityDetectingColliderExit = "EntityDetectingColliderExit";
    public const string PlayerHasBeenMissed = "PlayerHasBeenMissed";
    public const string PlayerEnteredTheRadiusOfMeleeAttack = "PlayerEnteredTheRadiusOfMeleeAttack";
    public const string PlayerExitTheRadiusOfMeleeAttack = "PlayerExitTheRadiusOfMeleeAttack";
}