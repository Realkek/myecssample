﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeleeAttackEvents
{
    public const string MeleeAttackIsOnAvailableNow = "MeleeAttackIsOnAvailableNow";
    public const string MeleeAttackIsNotAvailableNow = "MeleeAttackIsNotAvailableNow";
}